<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mywp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1:12345'); 
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b((E dSssm5k5cKHqYAMaF=mE7B<:wut3WYX#Ln6?}L.sdrM99OX Y5_2+FHG_2t' );
define( 'SECURE_AUTH_KEY',  'MD:=pn!?@zGfk/KmQP3ls0o^xq@DtDd6bF;Rq~-3+1=JWsK7s@>TNgshthw-Yu&w' );
define( 'LOGGED_IN_KEY',    'u6u*q7%}#E9`msuE@<;;H(YZqVRjs|TA(67 x`A5W:6N<gJ;oV4l3p6JD%yOL-QD' );
define( 'NONCE_KEY',        '@$q#]lZng:/~E3[;6%f=40`k.l^*dq5g%]+y}]9L=0s &~_Vz%V2uB U9NDwe4YQ' );
define( 'AUTH_SALT',        ')cY&65K[.pmeEnBq[}g7l9;H3K.a2J1i<4wdlLU^#gAyJ!tVo5>wYFP|=!C6U0j5' );
define( 'SECURE_AUTH_SALT', 'j~!-u?k]]X0+$d}L#94TH8!2GwEBg2yF{F4EtdCS:%>I+4ifH>i$<n#!eUs,E.:<' );
define( 'LOGGED_IN_SALT',   '6.Ug#M=>u?rZTLn8Ck7`|m#)( I.e#D-_RyxT4=6@G^$`^la]%x[@WU#t4{C2&-f' );
define( 'NONCE_SALT',       'gHJ+!RI-{*!OgbT/}8lH2?2( lX:s3ljqsyb&#H1hjxTk@NzX0RHu@K.?#%2d`)P' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
